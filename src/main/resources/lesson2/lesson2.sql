-- create messages
-- 1-to-M - users and messages
-- 1-to-M - channel and messages

-- create table channel_details (
-- 	channel_id bigint primary key (not null, unique),
--  people_count int not null,
-- 	description text, -- text - string
--  foreign key (channel_id) references channel(id)
-- );

create table messages (
                          id serial primary key,
                          text text not null,
                          date timestamp not null,
                          user_id integer not null,
                          channel_id integer not null,
                          constraint message_user_id_fkey foreign key (user_id) references users(id),
                          constraint message_channel_id_fkey foreign key (channel_id) references channel(id)
);

-- 1 - 2
-- 1 - 1
-- 2 - 1
-- 1 - 2 - x
create table user_channels (
                               user_id bigint,
                               channel_id bigint,
                               constraint user_channels_pkey primary key (user_id, channel_id),
                               constraint user_channels_user_id_fkey foreign key (user_id) references users(id),
                               constraint user_channels_channel_id_fkey foreign key (channel_id) references channel(id)
);

select * from channel;
select * from users;

insert into user_channels values
(1, 1),
(1, 2),
(1, 3),
(3, 1),
(3, 2),
(4, 1),
(4, 3),
(4, 5);

select * from user_channels;

insert into messages (text, date, user_id, channel_id)
values
('Hello!', now(), 1, 1),
('Hi!', now(), 3, 1),
('How are you?', '2020-05-25 20:06:05', 4, 1),
('Fine', '2020-05-25 20:12:05', 1, 1);

select * from messages;

-- first_name, last_name, text, date
select u.first_name, u.last_name, m.text, m.date from users u, messages m
where u.id = m.user_id; -- cross join

--
select u.first_name, u.last_name, m.text, m.date from messages m
-- join
                                                          inner join users u on u.id = m.user_id;

insert into users (login, first_name, last_name)
values
('seriva', 'Sergey', 'Ivanov');

select u.first_name, u.last_name, m.text, m.date from messages m
                                                          right outer join users u on u.id = m.user_id;

select u.first_name, u.last_name, m.text, m.date from users u
                                                          left outer join messages m on u.id = m.user_id;

select u.first_name, u.last_name from users u
                                          left outer join messages m on u.id = m.user_id
where m.id is null;


-- max(), min(), count(), avg(), sum()
select count(*) from users u
where u.first_name = 'Дмитрий';

select max(m.date), m.text from messages m
-- where
group by m.text;

select m.text from messages m
-- where m.date = max(m.date);
where m.date = (select max(m.date) from messages m);


-- first_name, last_name, message_count
select u.first_name, u.last_name, count(m.text) from users u
                                                         left join messages m on u.id = m.user_id
-- where
group by u.id
having count(m.text) > 1
order by count(m.text) desc
fetch first 10 rows only;
-- limit 10 offset 10
-- limit - количество строк, которые будут в результате
-- offset -

-- 1 record from 1 table - aggregated record from 2 table
-- count(*)


alter table messages alter column text drop not null;
-- alter table messages add column type int;
-- alter table messages drop column type;
-- alter table messages rename table
-- atler table messages rename column old_name to new_name

insert into messages (text, date, user_id, channel_id) values
(null, now(), 1, 1);

select * from messages;

select u.first_name, u.last_name, m.text, m.date from users u
                                                          left join messages m on u.id = m.user_id;

select count(m.text) from users u
                              left join messages m on u.id = m.user_id; -- 4

select count(m.*) from users u
                           left join messages m on u.id = m.user_id; -- 5

select count(*) from users u
                         left join messages m on u.id = m.user_id; -- 6



