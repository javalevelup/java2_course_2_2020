-- //
-- create table if not exists users ();
create table users (
    -- column name, type
    id serial primary key, -- primary key - not null unique, serial - bigint auto_increment
    login varchar(20) not null unique,
    first_name varchar(100) not null,
    last_name varchar(100)
);

-- drop table users;

insert into users (last_name, first_name, login)
values
    ('Иванов', 'Олег', 'oleiva'),
    ('Сидоров', 'Иван', 'ivasid');

insert into users (login, first_name, last_name)
values
    ('dmipro', 'Дмитрий', 'Процко'),
    ('dmitro', 'Дмитрий', 'Тротьев'),
    ('anavir', 'Анастасия', 'Вир');

-- select * from users where id in (2, 4, 5);
-- select * from users where login = 'dmitro' -> login == 'dmitro'
select * from users where login like '%dmi%';

update users set first_name = 'Алексеем', login = 'alesid' where login = 'ivasid';

select * from users;

update users set first_name = 'Алексей' where login = 'alesid';

delete from users where login like 'a%';

select * from users;

----
create table channel (
    id serial primary key,
    name varchar(100) not null unique,
    display_name varchar(100) not null
);

create table channel_details (
    channel_id bigint primary key,
    people_count int not null,
    description text, -- text - string
    foreign key (channel_id) references channel(id)
);

insert into channel (name, display_name)
values
    ('levelupjava2', 'LevelUp. Java2.'),
    ('pro.jvm', 'Pro.JVM');
select * from channel;

insert into channel_details
values
    (1, 14, 'Java 2. LevelUp course');

select * from channel_details;

delete from channel_details where channel_id = 1;
delete from channel where id = 1;

insert into channel (name, display_name)
values
    ('pro.algorithms.seven', 'Pro.Algorithms - Seven version')



-- 1 to 1
-- 1 to M
-- M to M

