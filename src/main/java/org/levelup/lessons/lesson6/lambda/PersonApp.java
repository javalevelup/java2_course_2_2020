package org.levelup.lessons.lesson6.lambda;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class PersonApp {

    public static void main(String[] args) {
        Collection<String> names = new ArrayList<>();

        names.add("John");
        names.add("Petr");
        names.add("Ivan");
        names.add("Dmitry");

//        for (String name : names) {
//            System.out.println(name);
//        }

        // Consumer - void accept(T value)
        // Supplier - T get();
        // Function - T apply(R value)
//        names.forEach(new Consumer<String>() {
//            @Override
//            public void accept(String s) {
//                System.out.println(s);
//            }
//        });

        //names.forEach((name) -> System.out.println(name));
        // names.forEach(name -> System.out.println(name));

        List<String> filteredNames = names.stream()
                .filter(name -> name.length() > 5)// Stream - "Dmitry"
                .map(name -> name.toUpperCase())
                .collect(Collectors.toList());
        filteredNames.forEach(name -> System.out.println(name));


    }

    // List<any class implements Comparable>
    @SuppressWarnings("ComparatorCombinators")
    static void sort(List<Person> people) {
        // org.levelup.lessons.lesson6.PersonApp$1
        Comparator<Person> personComparator = new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };

        // org.levelup.lessons.lesson6.PersonApp$2
        Comparator<Person> personComparator2 = new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };

        // (method arguments without types) -> { method body }
        Comparator<Person> lambdaComparator = (obj1, obj2) -> {
            return obj1.getName().compareTo(obj2.getName());
        };

        // когда одно выражение в lambda, то {} можно не ставить
        Comparator<Person> lambdaComparator2 = (obj1, obj2) -> obj1.getName().compareTo(obj2.getName());

        Collections.sort(people, lambdaComparator2);
    }

}
