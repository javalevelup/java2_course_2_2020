package org.levelup.lessons.lesson6;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.chat.domain.User;

@RequiredArgsConstructor
public class UserDao {

    private final SessionFactory factory;

    public User createUser(String login, String firstName, String lastName) {
        User user;
        try (Session session = factory.openSession()) {
            Transaction t = session.beginTransaction();

            user = new User(login, firstName, lastName); // -> Transient

            Integer id = (Integer) session.save(user);
            System.out.println("Generated ID: " + id);

            // session.persist(user);  // -> Persistent
            // Integer id = user.getId()
            // session.persist(user); -> ничего не произойдет во второй раз
            user.setFirstName(firstName + " (updated)");
            // user.setLastName(lastName + "(updated)");
            //  -> только 1 update, update users set first_name = "", last_name = ""

            t.commit();
        }
//        session.close() -> user - detached
//        user.setFirstName() - изменения не будут отправлены в базу
//        try (Session session = factory.openSession()) {
//            Transaction t = session.beginTransaction();
//
//            user.setLogin(user.getLogin() + "2");
//            user.setLastName(user.getLastName() + "(update detached object)");
//            Integer id = (Integer) session.save(user);
//            System.out.println("Generated ID of detached object: " + id);
//
//            t.commit();
//        }
        return user;
    }

    public User mergeUser(Integer id, String login, String firstName, String lastName) {
        User user;

        try (Session session = factory.openSession()) {
            // get() equivalent to createQuery("from User where id = :id).setParameter("id", id).uniqueResult()
            user = session.get(User.class, id); // select * from users where id = ?
        }
        // user - detached state
        try (Session session = factory.openSession()) {
            Transaction t = session.beginTransaction();

            user.setLogin(login);
            user.setFirstName(firstName);
            user.setLastName(lastName);

            // user.equals(mergedUser) = true
            // user != mergedUser
            User mergedUser = (User) session.merge(user);
            // user - detached
            // mergedUser - persistent
            System.out.println("Equals: " + user.equals(mergedUser));
            System.out.println("==: " + (user == mergedUser));

            t.commit();
        }

        return null;
    }

    public User updateUser(Integer id, String login, String firstName, String lastName) {
        User user;

        try (Session session = factory.openSession()) {
            Transaction t = session.beginTransaction();

            user = session.get(User.class, id); // persistent

            session.evict(user); // user - detached
            user.setLogin(login);
            user.setFirstName(firstName);
            user.setLastName(lastName);

            // user = new User(login, firstName, lastName);
            // user.setId(id);
            session.update(user);

            t.commit();
        }

        return null;
    }

    // get, load
    // Получить сущность по ID
    // select * from table where id = ?

    public void get(Integer id) {
        User user;
        try (Session session = factory.openSession()) {
            user = session.get(User.class, id); // select
            // System.out.println("Persistent user: " + user.toString());
            System.out.println("Class in get method: " + user.getClass().getName());
        }
        System.out.println("Detached user: " + user.toString());
    }

    public void load(Integer id) {
        User user;
        try (Session session = factory.openSession()) {
            user = session.load(User.class, id); // do nothing
            System.out.println("Before getLogin: " + user.getClass().getName());
            System.out.println(user.getLogin()); // select
            System.out.println("After getLogin:" + user.getClass().getName());
            // System.out.println("Persistent loaded user: " + user.toString());
        }
        System.out.println("Detached loaded user: " + user.toString());
    }

}
