package org.levelup.lessons.lesson4.reflection;

import java.lang.reflect.*;

@SuppressWarnings("ALL")
public class ReflectionApp {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException {
        Watch watchObject = new Watch();
        Watch watch = new Watch();

        // 1 var
        Class<?> watchClass = watchObject.getClass();
        Class<?> watchClass2 = watch.getClass();

        System.out.println(watchClass == watchClass2);

        // 2 var
        Class<?> watchFromLiteral = Watch.class;
        System.out.println(watchClass == watchFromLiteral);

        // ClassLoader
        ClassLoader watchClassLoader = watchClass.getClassLoader();
        System.out.println(watchClassLoader.getName());

        Class<?> integerClass = Integer.class;
        System.out.println("Integer class loader: " + integerClass.getClassLoader()); // bootstrap


        // Reflection
        // object of Class<?> - точка доступа к ReflectionAPI
        System.out.println("Name of class: " + watchClass.getName());
        int classModifiers = watchClass.getModifiers();
        System.out.println("Modifier of class: " + Modifier.toString(classModifiers));

        // Field
        Field[] publicFields = watchClass.getFields();
        System.out.println("Public fields of class Watch:");
        for (Field field : publicFields) {
            System.out.println(field.getName());
        }

        watchObject.radius = 10;

        // getField(), getFields() - только public fields
        Field radiusField = watchClass.getField("radius"); //  NoSuchFieldException
        radiusField.set(watchObject, 11); // Object obj, Object val

        System.out.println("Watch radius: " + watchObject.radius);

        // getDeclaredFields() - все поля
        Field declaredField = watchClass.getDeclaredField("diameter");
        declaredField.setAccessible(true);
        declaredField.set(watchObject, 22); // IllegalAccessException

        // field.get(obj) - вернет значение поля
        System.out.println("Watch diameter: " + declaredField.get(watchObject));

        // Integer.class, Integer.class - changeTime(Integer, Integer)
        // int.class, int.class - changeTime(int, int)
        Method changeTimeMethod = watchClass.getDeclaredMethod("changeTime", int.class, int.class);
        changeTimeMethod.setAccessible(true);
        Object result = changeTimeMethod.invoke(watchObject, 20, 15); // watchObject.changeTime(20, 15);
        System.out.println(result);

        Constructor<?> watchConstructor = watchClass
                .getDeclaredConstructor(String.class, int.class); // Watch(String, int)
        watchConstructor.setAccessible(true);
        Watch newWatch = (Watch) watchConstructor.newInstance("Apple Watch", 10);
        System.out.println(newWatch.radius);
        System.out.println(newWatch.watchName);

        // Class.forName("org.levelup.chat.jdbc.DatabaseConnectionFactory");
        //      -> Class<DatabaseConnectionFactory> clazz; clazz.newInstance();
    }

}
