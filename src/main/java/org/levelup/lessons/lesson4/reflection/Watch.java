package org.levelup.lessons.lesson4.reflection;

public class Watch {

    public int radius;
    public String watchName;
    private int diameter;

    private int hour;
    private int minute;

    public Watch() {}

    private Watch(String watchName, int radius) {
        this.watchName = watchName;
        this.radius = radius;
        this.diameter = radius * 2;
    }

    private void changeTime(int hour, int minute) {
        System.out.println("changeTime invoked...");
        this.hour = hour;
        this.minute = minute;
    }

}
