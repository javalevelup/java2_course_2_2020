package org.levelup.lessons.lesson4.annotation;

import java.lang.reflect.Field;
import java.util.Random;

public class Game {

    public static void main(String[] args) {
        Dice dice = createDice();
        System.out.println("Cube value: " + dice.getCubeValue());
        System.out.println("Second cube: " + dice.getSecondCube());
        System.out.println("Third cube: " + dice.getThirdCube());
    }

    static Dice createDice() {
        Class<?> diceClass = Dice.class;

        try {
            Dice object = (Dice) diceClass.getConstructor().newInstance();
            Field[] fields = diceClass.getDeclaredFields();
            for (Field field : fields) {
                RandomInt annotation = field.getAnnotation(RandomInt.class);
                if (annotation != null) {
                    Random random = new Random();

                    int min = annotation.min(); // 1
                    int max = annotation.max(); // 6

                    int randomInt = random.nextInt(max - min + 1) + min; // [min, max] - [1, 6]

                    field.setAccessible(true);
                    field.set(object, randomInt);
                }
            }
            return object;
        } catch (Exception exc) {
            throw new RuntimeException(exc);
        }
    }

}
