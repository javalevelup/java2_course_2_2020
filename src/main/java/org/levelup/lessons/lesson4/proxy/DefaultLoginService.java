package org.levelup.lessons.lesson4.proxy;

public class DefaultLoginService implements LoginService {

    @Override
    @ProfileExecutionTime
    public void login(String login, String password) {
        System.out.println("User " + login + " try to login..."); // method body
    }

}
