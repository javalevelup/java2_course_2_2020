package org.levelup.lessons.lesson4.proxy;

public interface LoginService {

    void login(String login, String password);

}
