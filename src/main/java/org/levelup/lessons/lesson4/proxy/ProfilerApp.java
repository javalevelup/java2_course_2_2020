package org.levelup.lessons.lesson4.proxy;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProfilerApp {

    public static void main(String[] args) {
        LoginService loginService = getLoginService();
        loginService.login("userlogin", "userpassword");
    }

    static LoginService getLoginService() {
        DefaultLoginService loginService = new DefaultLoginService();

        Method[] methods = loginService.getClass().getDeclaredMethods();
        for (Method method : methods) {
            ProfileExecutionTime annotation = method.getAnnotation(ProfileExecutionTime.class);
            if (annotation != null) {
                return (LoginService) Proxy.newProxyInstance(
                        loginService.getClass().getClassLoader(),
                        loginService.getClass().getInterfaces(),
                        new ProfilerInvocationHandler(loginService)
                );
            }
        }

        return loginService;
    }

}
