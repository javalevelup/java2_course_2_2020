package org.levelup.lessons.lesson4.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ProfilerInvocationHandler implements InvocationHandler {

    private DefaultLoginService loginService;

    public ProfilerInvocationHandler(DefaultLoginService loginService) {
        this.loginService = loginService;
    }

    // LoginService.login()
    //      -> ProfilerInvocationHandler.invoke()
    //              -> DefaultLoginService.login()
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        long startExecution = System.nanoTime();

        Object result = method.invoke(loginService, args); // loginService.login(String, String)

        long endExecution = System.nanoTime();
        System.out.println("Execution time: " + (endExecution - startExecution) + " ns");
        return result;
    }

}
