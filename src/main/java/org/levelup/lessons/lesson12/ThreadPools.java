package org.levelup.lessons.lesson12;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ThreadPools {

    // 2222239
    // 266840

    // OutOfMemory: unable to create native thread: possibly out of memory or process/resource limits reached
    public static void main(String[] args) throws InterruptedException {
//        long start = System.nanoTime();
//        for (int i = 0; i < 10; i++) {
//            new Thread(() -> System.out.println(Thread.currentThread().getName())).start();
//        }
//        System.out.println("Execution time: " + (System.nanoTime() - start));
//        long startWithoutThread = System.nanoTime();
//        for (int i = 0; i < 10; i++) {
//            System.out.println(i);
//        }
//        System.out.println("Execution time: " + (System.nanoTime() - startWithoutThread));

        // ThreadPool - пул потоков

        List<Future<Integer>> futureResults = new ArrayList<>();

//        ExecutorService executorService = Executors.newSingleThreadExecutor();
//        ExecutorService executorService = Executors.newFixedThreadPool(3);
//        ExecutorService executorService = Executors.newCachedThreadPool();
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(3);
        executorService.scheduleWithFixedDelay(
                () -> System.out.println("Periodic task"),
                100,
                100,
                TimeUnit.MILLISECONDS
        );

        for (int i = 0; i < 10; i++) {
            // executorService.submit(() -> System.out.println(Thread.currentThread().getName()));
            Future<Integer> futureResult = executorService.submit(new LoopIndexCallable(i));
            futureResults.add(futureResult);
        }

        Thread.sleep(3_900);

        futureResults.forEach(future -> {
            try {
                System.err.println(future.get());
            } catch (InterruptedException | ExecutionException exc){
                throw new RuntimeException(exc);
            }
        });

        executorService.shutdown();

    }

    static class LoopIndexCallable implements Callable<Integer> {

        private int index;

        public LoopIndexCallable(int index) {
            this.index = index;
        }

        @Override
        public Integer call() throws Exception {
            System.out.println(Thread.currentThread().getName());
            Thread.sleep(500);
            return index;
        }

    }

}
