package org.levelup.lessons.lesson5;

import org.levelup.chat.jdbc.DatabaseConnectionFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SelectFromConsole {

    public static void main(String[] args) throws SQLException, IOException {
        Connection connection = DatabaseConnectionFactory.createConnection();

        PreparedStatement statement = connection
                .prepareStatement("select * from users where login = ?");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter login:");
        statement.setString(1, reader.readLine());

        ResultSet set = statement.executeQuery();
        while (set.next()) {
            System.out.println(set.getString(1));
            System.out.println(set.getString(2));
            System.out.println(set.getString(3));
            System.out.println(set.getString(4));
        }
    }

}
