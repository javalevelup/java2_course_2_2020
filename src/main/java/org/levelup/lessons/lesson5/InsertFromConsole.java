package org.levelup.lessons.lesson5;

import org.levelup.chat.jdbc.DatabaseConnectionFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InsertFromConsole {

    public static void main(String[] args) throws SQLException, IOException {
        Connection connection = DatabaseConnectionFactory.createConnection();
        PreparedStatement statement = connection
                .prepareStatement("insert into users (login, first_name, last_name) values (?, ?, ?)");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter login:");
        String login = reader.readLine();
        statement.setString(1, login);

        System.out.println("Enter first name: ");
        statement.setString(2, reader.readLine());

        System.out.println("Enter last name:");
        statement.setString(3, reader.readLine());

        int rowAffected = statement.executeUpdate();
        System.out.println("New rows: " + rowAffected);
    }

}
