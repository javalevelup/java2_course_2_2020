package org.levelup.lessons.lesson11;

import java.util.concurrent.locks.ReentrantLock;

// Java 5
// java.util.concurrent.*
public class ReentrantCounter {

    private ReentrantLock lock;

    private int counter;

    public ReentrantCounter() {
        this.lock = new ReentrantLock(false); // true/false - честная/нечестная блокировка (дословно)
    }

    // synchronized (this) { counter++; }
    public void increase() {
        lock.lock(); // вход в критическую секцию (в блок, который синхронизирован)
        try {
            counter++;
        } finally {
            lock.unlock(); // отпускаем блокировку
        }
    }

    public int increaseAndGetReentrant() {
        lock.lock();
        counter++;
        return getReentrant();
    }

    private int getReentrant() {
        try {
            return counter;
        } finally {
            lock.unlock(); // ВАЖНО!
        }
    }

    public int increaseAndGet() {
        synchronized (this) {
            counter++;
            return get();
        }
    }

    public int get() {
        synchronized (this) {
            return counter;
        }
    }

}
