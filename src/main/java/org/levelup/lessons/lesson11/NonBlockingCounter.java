package org.levelup.lessons.lesson11;

// Блокирующие алгоритмы - алгоритмы, в которых используется блокировка
// Неблокирующие алгоритмы

import java.util.concurrent.atomic.AtomicInteger;

// java.util.concurrent.atomic.*
public class NonBlockingCounter {

    private AtomicInteger atomicCounter = new AtomicInteger(0);

    // private AtomicReference<Integer> reference;// ~ AtomicInteger

    public void increase() {
        // CAS - compare and set

        // Есть цикл
        //      int valueBeforeOps = get();
        //      int value = valueBeforeOps;
        //      value++;
        // int valueAfterOps = get()
        // if (valueAfterOps == valueBeforeOps) -> set(value)
        // else повторяем итерацию
        atomicCounter.incrementAndGet();
    }

}
