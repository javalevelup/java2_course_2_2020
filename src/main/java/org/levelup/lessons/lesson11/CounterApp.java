package org.levelup.lessons.lesson11;

// biased - one CAS  -XX:BiasedLockingStartupDelay=0
// thin - CAS на каждом обращении потока к объетку
// fat - OS + mutex

public class CounterApp {

    public static void main(String[] args) {

        ReentrantCounter counter = new ReentrantCounter();

        // thread contention

        // Thread-2 3
        //Thread-0 2
        //Thread-1 1
        //Thread-1 4
        //Thread-0 5
        //Thread-2 6
        //Thread-0 7
        //Thread-2 8
        //Thread-1 9
        //Thread-0 10
        for (int k = 0; k < 3; k++) {
            new Thread(() -> {
                for (int i = 0; i < 20; i++) {
                    System.err.println(Thread.currentThread().getName() + " " + counter.increaseAndGetReentrant());
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException exc) {
                        throw new RuntimeException(exc);
                    }
                }
            }).start();
        }

    }

}
