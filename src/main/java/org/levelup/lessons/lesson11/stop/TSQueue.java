package org.levelup.lessons.lesson11.stop;

// FIFO - first in, first out
public interface TSQueue<T> {

    void put(T element);

    T take();

    boolean isEmpty();

}
