package org.levelup.lessons.lesson11.stop;

import lombok.RequiredArgsConstructor;

// Генерировать набор задач, которые нужно будет выполнить
@RequiredArgsConstructor
public class ProducerThread extends Thread {

    // boolean shutdown

    // t.isInterrupted()
    // t.interrupt()
    // Thread.interrupted()

    private final TSQueue<String> queue;

    @Override
    public void run() {
        int counter = 0;
        while (true) {
            if (Thread.interrupted()) {
                System.out.println("Поток попросили остановиться");
                if (counter > 15) {
                    System.out.println("Поток остановился");
                    break;
                } else {
                    System.out.println("Поток не создал 25 задач, поэтому не будет останавливать его");
                }
            }

            System.out.println("Генерируем задачу " + getName() + "#" + ++counter);
            queue.put(getName() + "#" + counter);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                interrupt();
            }
        }


//        try {
//            while (!isInterrupted()) {
//                System.out.println("Генерируем задачу #" + counter++);
//                Thread.sleep(100);
//            }
//        } catch (InterruptedException exc) {
//            System.out.println("Поток был разбужен");
//        }
//

//        while (!isInterrupted()) {
//            System.out.println("Генерируем задачу #" + counter++);
//            try {
//                Thread.sleep(100);
//            } catch (InterruptedException e) {
//                interrupt();
//            }
//        }
    }

}
