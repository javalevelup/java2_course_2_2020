package org.levelup.lessons.lesson11.stop;

import lombok.SneakyThrows;

public class App {

    // Thread states
    // NEW - поток создан, но еще не запущен (метод start() не вызван)
    // RUNNABLE - поток запущен, готов выполниться
    // RUNNING - поток запушен, выполняется
    // BLOCKED - поток прерван, ожидает доступа к ресурсу (ожидает монитор)
    // WAITING - поток прерван, ожидает события, которое разбудит этот поток
    // TIMED_WAITING - поток прерван, ожидает события, которое разбудит этот поток, но ждет ограниченное время
    // TERMINATED - поток завершен

    @SneakyThrows
    public static void main(String[] args) {
        TSQueue<String> queue = new ReentrantQueue<>(10);
        ProducerThread pt1 = new ProducerThread(queue);
        pt1.setName("Producer #1");
        ProducerThread pt2 = new ProducerThread(queue);
        pt2.setName("Producer #2");

        ConsumerThread ct1 = new ConsumerThread(queue);
        ct1.setName("Consumer #1");
        ConsumerThread ct2 = new ConsumerThread(queue);
        ct2.setName("Consumer #2");

        ct1.start();
        ct2.start();

        pt1.start();
        pt2.start();

        Thread.sleep(8_000);
        ct1.needShutdown = true;
        ct2.startShutdown();
        pt1.interrupt();
        pt2.interrupt();

//        System.out.println("До вызова метода start(): " + producerThread.getState().name());
//        producerThread.start();
//
//        Thread.sleep(1000);
//        System.out.println("Просим поток остановиться");
//        producerThread.interrupt(); // shutdown -> true
//
//        Thread.sleep(2000);
//        System.out.println("Просим поток остановиться во второй раз!");
//        producerThread.interrupt();

    }

}
