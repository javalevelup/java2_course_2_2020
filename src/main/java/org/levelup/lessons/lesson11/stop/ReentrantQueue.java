package org.levelup.lessons.lesson11.stop;

import lombok.SneakyThrows;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantQueue<T> implements TSQueue<T> {

    private final Queue<T> queue;
    private final int size;

    private final ReentrantLock lock;
    private final Condition queueFull;
    private final Condition queueEmpty;

    public ReentrantQueue(int maxSize) {
        this.size = maxSize;
        this.queue = new LinkedList<>();

        this.lock = new ReentrantLock();
        this.queueFull = lock.newCondition();
        this.queueEmpty = lock.newCondition();
    }

    @Override
    @SneakyThrows
    public void put(T element) {
        lock.lock();
        try {
            while (size == queue.size()) {
                // ожидать пока не освобидтся место в очереди
                queueFull.await(1, TimeUnit.SECONDS);
            }

            queue.add(element);
            queueEmpty.signalAll();
        } finally {
            lock.unlock();
        }
    }

    @Override
    @SneakyThrows
    public T take() {
        lock.lock();
        try {
            while (queue.isEmpty()) {
                queueEmpty.await(1, TimeUnit.SECONDS);
            }

            T element = queue.remove();
            queueFull.signalAll();
            return element;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean isEmpty() {
        return queue.isEmpty();
    }
}
