package org.levelup.lessons.lesson11.stop;

import lombok.SneakyThrows;

import java.util.LinkedList;
import java.util.Queue;

// wait() - прерывает поток
// wait(time)
// notify()
// notifyAll()
public class SynchronizedQueue<T> implements TSQueue<T> {

    private final Queue<T> syncQueue;
    private final int size;

    private final Object queueFull = new Object();
    private final Object queueEmpty = new Object();

    public SynchronizedQueue(int maxSize) {
        this.size = maxSize;
        this.syncQueue = new LinkedList<>();
    }

    @Override
    @SneakyThrows
    public void put(T element) {
        synchronized (queueFull) {
            while (size == syncQueue.size()) {
                // Должны ждать, пока в очереди освободится место
                System.out.println(Thread.currentThread().getName() + ": wait");
                queueFull.wait();
                System.out.println(Thread.currentThread().getName() + ": wake up");
            }
        }

        synchronized (queueEmpty) {
            syncQueue.add(element); // добавляется в конец очереди
            queueEmpty.notifyAll();
        }
    }

    @Override
    @SneakyThrows
    public T take() {
        synchronized (queueEmpty) {
            while (syncQueue.isEmpty()) {
                System.out.println(Thread.currentThread().getName() + ": wait");
                queueEmpty.wait();
                System.out.println(Thread.currentThread().getName() + ": wake up");
            }
        }

        synchronized (queueFull) {
            T element = syncQueue.remove(); // удаляем элемент с начала очереди
            queueFull.notifyAll();
            return element;
        }
    }

    @Override
    public boolean isEmpty() {
        return syncQueue.isEmpty();
    }

}
