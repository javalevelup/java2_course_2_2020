package org.levelup.lessons.lesson11.stop;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ConsumerThread extends Thread {

    private final TSQueue<String> queue;

    // Мы не можем завершить поток, если в очереди есть задачи

    // Кэши процессора
    // Уровни кэшей процессора
    // 3 уровня
    //  L1
    //  L2
    //  L3

    // 1. Говорит потоку, постоянно брать значение из памяти
    // 2. Убирает некоторые оптимизации компилятора
    // 3. Не дает делать reordering инструкций
    // 4. Обеспечивает happens-before (любое изменение переменной будет видно из других потоков)
    // 5. Всегда нужно использовать volatile при доступе к long/double
    public volatile boolean needShutdown;

    @Override
    public void run() {
        // if needShutdown -> проверить размер очереди -> если не пуста, то работаем дальше
        // if !needShutdown -> то просто работаем дальше
        while (!needShutdown || !queue.isEmpty()) {
            try {
                System.out.println("Пытаемся забрать задачу из очереди");
                System.out.println(queue.take());
                Thread.sleep(100);
            } catch (Exception exc) {
                System.out.println("Было выброшено исключение " + exc.getClass().getName() + ": " + exc.getMessage());
            }
        }
        System.out.println(Thread.currentThread().getName() + " закончил работу");
//        while (!isInterrupted()) {
//            System.out.println(queue.take());
//            try {
//                Thread.sleep(200);
//            } catch (InterruptedException exc) {
//                interrupt();
//            }
//        }
    }

    public void startShutdown() {
        this.needShutdown = true;
    }

    public void cancelShutdown() {
        this.needShutdown = false;
    }


}
