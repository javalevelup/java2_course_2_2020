package org.levelup.lessons.lesson3;

import org.levelup.chat.jdbc.DatabaseConnectionFactory;

import java.sql.*;

public class JdbcApp {

    public static void main(String[] args) throws SQLException {

        Connection connection = DatabaseConnectionFactory.createConnection();

        // SQL-Injection
        //  String login = "drop database";
        //  insert into users (login) values ( + login + )
        PreparedStatement insertUser = connection.prepareStatement(
                "insert into users (login, first_name, last_name) values (?, ?, ?)"
        );

        insertUser.setString(1, "dmidmi");
        insertUser.setString(2, "Дмитрий");
        insertUser.setString(3, "Дмитриев");

        int rowInserted = insertUser.executeUpdate();
        System.out.println("Изменили " + rowInserted + " строк(у)");


        // String login = "oleole";
        // String sql = "insert into users (login) values ('" + login + "')";
        // insert into users (login) values ('oleole')

        // JDBC
        // Connection
        // Statement <- PreparedStatement <- CallableStatement
        // ResultSet

        Statement selectAllUsers = connection.createStatement();
        // execute -> boolean - true/false
        // executeQuery -> ResultSet - select queries
        // executeUpdate -> int - insert/update/delete
        ResultSet result = selectAllUsers.executeQuery("select * from users");
        // boolean next()

        while (result.next()) {
            int id = result.getInt(1); // Integer id = result.getInt(1)
            String login = result.getString("login");
            String firstName = result.getString(3);
            String lastName = result.getString("last_name");

            System.out.println(id + " | " + login + " | " + firstName + " | " + lastName);
        }

        connection.close();

    }

}
