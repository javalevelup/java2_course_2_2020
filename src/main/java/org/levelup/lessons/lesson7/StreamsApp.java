package org.levelup.lessons.lesson7;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StreamsApp {

    public static void main(String[] args) {

        // Optional.of(null) // NPE
//        if (args != null) {
//            args = new String[] {"argument1"};
//        } else {
//            args = new String[0];
//        }
//

        String[] arg = Optional.ofNullable(args)
                .map(arguments -> new String[]{"argument1"})
                .orElse(new String[0]);


        // StreamAPI + Lambda
        Collection<String> logins = new ArrayList<>();

        logins.add("qwerty");
        logins.add("asdfgh");
        logins.add("tim");
        logins.add("dim");
        logins.add("dim2");
        logins.add("poplut43");

//        logins.stream()
//                .map(login -> {
//                    System.out.println(login);
//                    return login.length();
//                });

        // filters
        // logins, which length < 6

        // until 8 Java
        Collection<String> shortLogins = new ArrayList<>();
        for (String login : logins) {
            if (login.length() < 6) {
                shortLogins.add(login);
            }
        }

        int maxLengthOfShortLogin = 6;
        // in 8 Java
        Collection<String> shortLoginsUseStream = logins.stream()
                // Используем класс, который явно объявлен
                //.filter(new ShortLoginPredicate()) // если выражение возвращает true, то тогда элемент остается в коллекции

                // Используем аннонимный внутренний класс
//                .filter(new Predicate<String>() {
//                    @Override
//                    public boolean test(String s) {
//                        return s.length() < maxLengthOfShortLogin;
//                    }
//                })

                // Используем lambda
                //  lambd: (args without types) -> { override method body }
//                .filter((string) -> {
//                    return string.length() < maxLengthOfShortLogin;
//                })
                .filter(string -> string.length() < maxLengthOfShortLogin) // method filter: Stream<T> filter(Predicate<T> p)
                .collect(Collectors.toList()); // Stream<String> -> List<String>

        System.out.println(shortLogins);
        System.out.println("Filtered using stream: " + shortLoginsUseStream);

        // Stream types:
        //      - Stream
        //      - IntStream
        //      - LongStream
        //      - DoubleStream

        // Find min string length
        //  transform Stream<String> to Stream<Integer> and find min
        int minLoginLength = logins.stream() // Stream<String>

                // arg -> expression(arg)
                // forEach(el -> System.out.println(el)); -> System.out::println

                // arg -> arg.method() -> ArgClass::method -> method reference (ссылка на метод)
                // map(string -> string.toUpperCase()) -> String::toUpperCase

                // mapToInt(login -> login.length() * 2) -> нельзя использовать MethodReference
                // mapToInt(login -> login.length()) -> String::length
                .mapToInt(String::length) // Function<T,R> -> R apply(T t); -> Stream<Integer>: 6, 6, 3, 3, 4, 7 -> IntStream
                .min()
                .orElse(0);
        System.out.println("Min login length: " + minLoginLength);

        // Find min login and display it with length
        //noinspection ComparatorCombinators
        String minLogin = logins.stream()
                .min((fLogin, sLogin) -> Integer.compare(fLogin.length(), sLogin.length()))
                .orElse(null);

        // Group logins by length
        //  Collection<String> -> Map<Integer, List<String>>
        Map<Integer, List<String>> groupedByLength = logins.stream()
                .collect(Collectors.groupingBy(String::length)); // Map<Integer, List<String>>

        // Find min map key
        int minLengthKey = groupedByLength.keySet().stream() // get all keys in map, Set<Integer> -> Stream<Integer> -> IntStream
                // .mapToInt(key -> key) // Integer -> int: integerValue
                // .mapToInt(key -> key.intValue()) // Integer -> int: integerValue
                .mapToInt(Integer::intValue)
                .min()
                .orElse(0);

        List<String> loginsWithTheShortestLength = groupedByLength.get(minLengthKey);

        // Integer integerValue
        // int intValue = integerValue; int intValue = integerValue.intValue();

        List<String> strings = logins.stream()
                .collect(Collectors.groupingBy(String::length))
                .entrySet()
                .stream()
                .min(Comparator.comparingInt(Map.Entry::getKey))
                .map(Map.Entry::getValue)
                .orElse(null);

    }

    static class ShortLoginPredicate implements Predicate<String> {
        @Override
        public boolean test(String s) {
            return s.length() < 6; // true, if string length is [0, 5]
        }
    }

}
