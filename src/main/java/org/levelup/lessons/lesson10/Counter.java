package org.levelup.lessons.lesson10;

// ThreadSafe (потокобезопасность)
//  Критическия секция - это участок кода, который может выполнять только один поток в единый момент времени
//  synchronized - это описание блока кода
//      synchronized method - здесь блокировка берется по this.
//      synchronized block
public class Counter {

    private final Object object = new Object();
    private final Object obj = new Object();

    private int counter; // счетчик каких-то вычислений или вызовов

    // counter.increase()
    public void increase() {
        synchronized (this) { // counter 1 t1
            System.err.println(Thread.currentThread().getName() + ", значение в методе increase: " + getCounter());
            counter++;
            // 1 операция increment - 3 простых простых операции
            //  чтение из памяти
            //  выполнение ++
            //  запись в память
            System.err.println(Thread.currentThread().getName() + ", значение счетчика: " + getCounter());
        }
    }

    // t1/t2 c1
    //  t1 -> some code
    //  t1 -> c1.getCounter()
    //  t1 -> some code

    //  t2 -> c1.printCounter()
    //  t2 -> c1.getCounter()
    //  t2 -> some code

    // counter.getCounter()
    public int getCounter() {
        synchronized (this) {
            return counter;
        }
    }

    public synchronized void printCounter() { // counter.printCounter()
        //synchronized (object) {
            System.out.println("Значение счетчика: " + counter);
        //}
    }

}
