package org.levelup.lessons.lesson10;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@RequiredArgsConstructor
public class CounterRunnable implements Runnable {

    // private boolean shutdown;

    private final Counter counter;

    @Override
    @SneakyThrows
    public void run() {
        for (int i = 0; i < 20; i++) {
            counter.increase();
            // Thread.sleep(200); // 200 ms

        }
    }

}
