package org.levelup.lessons.lesson10;

import lombok.SneakyThrows;

public class CounterApp {

    @SneakyThrows
    public static void main(String[] args) {
        Counter c1 = new Counter();
        Counter c2 = new Counter();

        Thread t1 = new Thread(new CounterRunnable(c1), "Поток t1");
        Thread t2 = new Thread(new CounterRunnable(c1), "Поток t2");
        Thread t3 = new Thread(new CounterRunnable(c1), "Поток t3");

        t1.start();
        t2.start();
        t3.start();

        // Остановим поток main, пока потоки t1 и t2 не завершатся
        t1.join();
        t2.join();
        t3.join();

        System.out.println("c1.value: " + c1.getCounter());
        // System.out.println("c2.value: " + c2.getCounter());

    }

}
