package org.levelup.lessons.lesson10.upload;

import org.levelup.chat.domain.Channel;

import java.util.ArrayList;
import java.util.Collection;

// b.method(a.method2())
public class UploaderApp {

    public static void main(String[] args) {
        Channel c1 = new Channel();
        Channel c2 = new Channel();
        System.out.println(c1.equals(c2));

        ChannelsUploader uploader = new ChannelsUploader();

        Collection<Channel> channels = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            channels.add(new Channel("channel" + i));
        }
        uploader.uploadChannels(channels);

    }

}
