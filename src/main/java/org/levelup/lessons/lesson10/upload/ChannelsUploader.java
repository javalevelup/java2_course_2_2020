package org.levelup.lessons.lesson10.upload;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.levelup.chat.domain.Channel;

import java.util.Collection;
import java.util.Iterator;

public class ChannelsUploader {

    // Загрузить каналы
    // НО! У нас ограниченное количество потоков: 3

    // Каждый поток должен брать из collection следующий объект, который нужно загрузить

    public void uploadChannels(Collection<Channel> channels) {
        Iterator<Channel> iterator = channels.iterator();
        ChannelIterator cIterator1 = new ChannelIterator(iterator);
        ChannelIterator cIterator2 = new ChannelIterator(iterator);
        ChannelIterator cIterator3 = new ChannelIterator(iterator);

        new Thread(new ChannelUploadWorker(cIterator1)).start();
        new Thread(new ChannelUploadWorker(cIterator2)).start();
        new Thread(new ChannelUploadWorker(cIterator3)).start();
    }

    @RequiredArgsConstructor
    private class ChannelUploadWorker implements Runnable {

        private final ChannelIterator channelIterator;

        @Override
        @SneakyThrows
        public void run() {
            // t1: hasNext() return true
            // t2: hasNext() return true
            // t1: next() return channel3
            // t2: next() -> throw NoSuchElementException

            Channel c;
            while ( (c = channelIterator.getNextOrNull()) != null ) { // true
                System.out.println(Thread.currentThread().getName() + ": upload channel " + c.getName() + " to database");
            }
        }

    }

    @RequiredArgsConstructor
    private class ChannelIterator {

        private final Iterator<Channel> iterator;
//        synchronized Channel next() { // блокируем channelIterator
//            return iterator.next();
//        }
//
//        synchronized boolean hasNext() { // блокируем channelIterator
//            return iterator.hasNext();
//        }
        //synchronized Channel getNextOrNull() {
        Channel getNextOrNull() {
            synchronized (iterator) {
                return iterator.hasNext() ? iterator.next() : null;
            }
        }

    }

}
