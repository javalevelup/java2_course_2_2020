package org.levelup.lessons.lesson8;

public class StringUtils {

    // true - строка у нас null или пустая ("") или состоит из одних проблелов ("     ")
    public static boolean isBlank(String str) {
        // "     ".trim() -> ""
        // "   434".trim() -> "434"
        // "434   ".trim() -> "434"
        // "   434   ".trim() -> "434"
        return str == null || str.trim().isEmpty();
    }

}
