package org.levelup.lessons.lesson9;

import lombok.SneakyThrows;

public class Threads {

    @SneakyThrows
    public static void main(String[] args) {

        // class extends Thread
        // class implements Runnable
        //  since 5 Java - class implements Callable

        // main thread - поток, в котором запускается метод main(String[] args)
        Thread background = new Thread(new SleepingRunnable());
        background.setDaemon(true); // теперь наш поток является daemon
        background.start();

        SoutThread t = new SoutThread();
        // t.run(); // простой вызов метода run, не создает поток, а просто запускает метод run в текущем потоке
        t.start(); // запуск нового потока
        t.join();
        // t.stop(); - НИКОГДА // sudo kill -9 65456

        for (int i = 0; i < 5; i++)
            System.out.println(Thread.currentThread().getName() + ": after t.start()");
    }

    static class SleepingRunnable implements Runnable {

        @Override
        @SneakyThrows
        public void run() {
            while (true) {
                System.out.println(Thread.currentThread().getName() + " wake up");
                Thread.sleep(1000);
            }
        }

    }

    static class SoutThread extends Thread {

        @SneakyThrows
        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + ": I love concurrency...");
            Thread.sleep(5000);
            System.out.println("The end...");
        }
    }

}
