package org.levelup.chat.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

// pr1_vip_accounts - > @Table(name="pr1_vip_accounts") Accounts

// Entity - сущность

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "channel")
public class Channel { // channel

    // IDENTITY
    //  insert() -> next_val('seq'), name, displayName

    // SEQUENCE
    //  id = select next_val('seq'), insert() -> id, name, displayName
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false, unique = true)
    private String name;
    @Column(name = "display_name", nullable = false, unique = false, length = 100) // display_name varchar(100) not null
    private String displayName;

    @OneToOne(mappedBy = "channel")
    private ChannelDetails details;

    @OneToMany(mappedBy = "channel")
    private Collection<Message> messages;

    @ManyToMany
    @JoinTable(
            name = "user_channels",
            joinColumns = @JoinColumn(name = "channel_id"), // колонка, которая ссылается на текущую таблицу (на таблицу channel)
            inverseJoinColumns = @JoinColumn(name = "user_id") // колонка, которая ссылается вторую таблицу из связи M-to-M
    )
    private Collection<User> users;

    public Channel(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Channel channel = (Channel) o;
        return Objects.equals(id, channel.id) &&
                Objects.equals(name, channel.name) &&
                Objects.equals(displayName, channel.displayName) &&
                Objects.equals(details, channel.details) &&
                Objects.equals(messages, channel.messages) &&
                Objects.equals(users, channel.users);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, displayName, details, messages, users);
    }

    // public Channel() {}
    // getters and setters

}
