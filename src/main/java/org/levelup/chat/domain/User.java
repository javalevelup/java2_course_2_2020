package org.levelup.chat.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

// @RequiredArgsConstructor - создает констркутор, в котором описываются все final поля
// class A {
//  private int var1;
//  private final int var2;
//  public A(int var2) { this.var2 = var2;}
// }

@Data   // = @Setter, @Getter, @EqualsAndHashCode, @ToString, @RequiredArgsConstructor
@Entity
@NoArgsConstructor
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String login;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;

    public User(String login, String firstName, String lastName) {
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
    }

}
