package org.levelup.chat.dao.impl;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.chat.dao.MessageDao;
import org.levelup.chat.domain.Channel;
import org.levelup.chat.domain.Message;
import org.levelup.chat.domain.User;

import java.time.LocalDateTime;

@RequiredArgsConstructor
public class HibernateMessageDao implements MessageDao {

    private final SessionFactory factory;

    @Override
    public void sendMessage(User user, Channel channel, String text) {
        try (Session session = factory.openSession()) {
            Transaction transaction = session.beginTransaction();

            Message message = new Message();
            message.setChannel(channel);
            message.setUser(user);
            message.setText(text);
            message.setDate(LocalDateTime.now());

            session.persist(message);

            transaction.commit();
        }
    }

}
