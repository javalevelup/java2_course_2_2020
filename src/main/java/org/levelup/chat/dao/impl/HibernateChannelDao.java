package org.levelup.chat.dao.impl;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.levelup.chat.dao.ChannelDao;
import org.levelup.chat.domain.Channel;
import org.levelup.chat.domain.ChannelDetails;
import org.levelup.chat.domain.User;
import org.levelup.chat.hibernate.HibernateUtils;

import java.util.Collection;
import java.util.List;

// DAO - Data Access Object
//  -> 5 methods - CRUD (create, read, update, delete)
//          -> create
//          -> update
//          -> delete
//          -> findById (getById)
//          -> findAll (getAll)

// Repository
//  -> find data in database
//      -> findByName (getByName)
//      -> findByField (getByField)
@RequiredArgsConstructor
public class HibernateChannelDao implements ChannelDao {

    private final SessionFactory factory;

    @Override
    public Channel createChannel(String name, String displayName) {
        Session session = factory.openSession(); // open new connection to database
        // Insert, update, delete
        Transaction transaction = session.beginTransaction();

        Channel channelWithTheSameName = findByName(name);
        if (channelWithTheSameName != null) {
            transaction.rollback();  // откатываем транзакцию до первоначального значения
            session.close();
            throw new IllegalArgumentException("The channel with name " + name + " exists.");
        }

        Channel channel = new Channel();
        channel.setName(name);
        channel.setDisplayName(displayName);

        session.persist(channel); // insert into channel, add new row into table "channel"

        transaction.commit();
        session.close(); // close connection to database

        return channel;
    }

    @Override
    public ChannelDetails appendDetails(Integer channelId, int peopleCount, String description) {
        try (Session session = factory.openSession()) {
            Transaction transaction = session.beginTransaction();

            Channel channel = session.get(Channel.class, channelId);

            ChannelDetails details = new ChannelDetails();
            details.setChannelId(channelId);
            details.setChannel(channel);
            details.setDescription(description);
            details.setPeopleCount(peopleCount);

            session.persist(details);

            transaction.commit();

            return details;
        }

    }

    @Override
    public Collection<Channel> findAll() {
        // Session implements AutoClosable
        // если класс/интерфейс реализуют интерфейс AutoClosable, то тогда этот класс/интерфейс можно использовать
        // в try-with-resources
        // try (Session session = factory.openSession()) {
        //    return session.createQuery("from Channel", Channel.class)
        //            .getResultList();
        // }

        Session session = factory.openSession();
        // HQL - Hibernate Query Language, similar to SQL
        // SQL: select * from channel
        // HQL: from Channel <- class name
        Collection<Channel> allChannels = session.createQuery("from Channel", Channel.class)
                .getResultList();
        session.close();
        return allChannels;
    }

    @Override
    public Channel findByName(String name) {
        try (Session session = factory.openSession()) {
            // SQL:  select * from channel where name = ?
            List<Channel> channels = session.createQuery("from Channel where name = :channelName", Channel.class)
                    .setParameter("channelName", name)
                    .getResultList();

//            Query<Channel> query = session.createQuery("from Channel where name = :channelName", Channel.class)
//                    .setParameter("channelName", name);
//            Channel channel = query.getSingleResult();

            return channels.isEmpty() ? null : channels.get(0);
        }
    }

    @Override
    public void addUserToChannel(Integer channelId, Integer userId) {
        try (Session session = factory.openSession()) {
            // ACID
            //  Атомарность
            //  Консистентность (Согласованность данных)
            //  Изоляция
            //  Durability (Стойкость)
            Transaction transaction = session.beginTransaction();

            Channel channel = session.get(Channel.class, channelId);

            User user = new User();
            user.setId(userId);
            channel.getUsers().add(user);

            session.merge(channel);

            transaction.commit();
        }
    }
}
