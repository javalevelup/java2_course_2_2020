package org.levelup.chat.dao;

import org.levelup.chat.domain.Channel;
import org.levelup.chat.domain.User;

public interface MessageDao {

    void sendMessage(User user, Channel channel, String text);

}
