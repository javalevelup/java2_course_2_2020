package org.levelup.chat.dao;

import org.levelup.chat.domain.Channel;
import org.levelup.chat.domain.ChannelDetails;

import java.util.Collection;

public interface ChannelDao {

    // method name "create..." -> метод должен вернуть то, что он создал
    Channel createChannel(String name, String displayName);

    ChannelDetails appendDetails(Integer channelId, int peopleCount, String description);

    Collection<Channel> findAll();

    Channel findByName(String name);

    void addUserToChannel(Integer channelId, Integer userId);

}
