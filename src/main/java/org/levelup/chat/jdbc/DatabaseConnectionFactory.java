package org.levelup.chat.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnectionFactory {

    // JDBC - Java Database Connectivity
    // API для подключения к базе

    // DriverManager
    //  сканировать драйвера
    //  регистрирует

    static {
        try {
            Class.forName("org.postgresql.Driver"); // implements Driver
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    // Connection
    public static Connection createConnection() throws SQLException {
        // url: jdbc:<vendor_name>://<host>:<port>/<database_name>?<connection_arguments>
        // ?useSSL=false&serverTimezone=UTC
        return DriverManager.getConnection(
                // localhost - 127.0.0.1
                "jdbc:postgresql://localhost:5432/chat", // database connection url
                "postgres",
                "кщще"
        );
    }

}
