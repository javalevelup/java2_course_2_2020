package org.levelup.chat;

import lombok.SneakyThrows;
import org.levelup.chat.dao.ChannelDao;
import org.levelup.chat.dao.impl.HibernateChannelDao;
import org.levelup.chat.hibernate.HibernateUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ChatApplication {

    @SneakyThrows
    public static void main(String[] args) {
        ChannelDao channelDao = new HibernateChannelDao(HibernateUtils.getFactory());

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите ID канала:");
        Integer channelId = Integer.parseInt(reader.readLine());

        System.out.println("Введите ID пользователя:");
        Integer userId = Integer.parseInt(reader.readLine());

        channelDao.addUserToChannel(channelId, userId);

//        Collection<Integer> col = new ArrayList<>();
//        for (Integer integer : col) {
//            col.remove(4); // ConcurrentModificationException
//        }

//        System.out.println("Enter channel ID:");
//        int channelId = Integer.parseInt(reader.readLine());
//
//        System.out.println("Enter user ID:");
//        int userId = Integer.parseInt(reader.readLine());
//
//        Channel channel = new Channel();
//        channel.setId(channelId);
//
//        User user = new User();
//        user.setId(userId);
//
//        MessageDao messageDao = new HibernateMessageDao(HibernateUtils.getFactory());
//
//        System.out.println("Enter message text:");
//        String text = reader.readLine();

//        messageDao.sendMessage(user, channel, text);

//        System.out.println("Enter people count:");
//        int peopleCount = Integer.parseInt(reader.readLine());
//        System.out.println("Enter description:");
//        String description = reader.readLine();
//
//        ChannelDetails details = channelDao.appendDetails(channelId, peopleCount, description);
//        System.out.println(details);

//        System.out.println("Enter channel name:");
//        String name = reader.readLine();
//
//        Channel channel = channelDao.findByName(name);
//        System.out.println(channel);

// findAll
//        Collection<Channel> allChannels = channelDao.findAll();
//        for (Channel channel : allChannels) {
//            System.out.println(channel);
//        }

// createChannel
//        System.out.println("Enter channel name:");
//        String name = reader.readLine();
//
//        System.out.println("Enter channel display name:");
//        String displayName = reader.readLine();
//
//        Channel channel = channelDao.createChannel(name, displayName);
//        System.out.println(channel);

        HibernateUtils.getFactory().close();
    }

}
