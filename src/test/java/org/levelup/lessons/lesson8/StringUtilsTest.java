package org.levelup.lessons.lesson8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class StringUtilsTest {

    // test methods
    // test<Method name>_when<Input params>_then<Result>
    @Test
    @DisplayName("Return true if string is null")
    public void testIsBlank_whenStringIsNull_thenReturnTrue() {
        boolean result = StringUtils.isBlank(null);
        Assertions.assertTrue(result); // бросит AssertionError, если result равен false
    }

    @Test
    @DisplayName("Return true if string is empty")
    public void testIsBlank_whenStringIsEmpty_thenReturnTrue() {
        Assertions.assertTrue(StringUtils.isBlank(""));
    }

    @Test
    @DisplayName("Return true if string consists only from whitespaces")
    public void testIsBlank_whenStringConsistsOnlyFromWhitespaces_thenReturnTrue() {
        Assertions.assertTrue(StringUtils.isBlank("    "));
    }

    @Test
    @DisplayName("Return false if string is not empty")
    public void testIsBlank_whenStringIsNotEmpty_thenReturnFalse() {
        Assertions.assertFalse(StringUtils.isBlank("not empty string"));
    }

}
