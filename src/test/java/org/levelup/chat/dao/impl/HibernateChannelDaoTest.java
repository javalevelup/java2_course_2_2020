package org.levelup.chat.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.levelup.chat.domain.Channel;
import org.mockito.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*; // eq() = ArgumentMatchers.eq();

class HibernateChannelDaoTest {

    @Mock // factory = Mockito.mock(SessionFactory.class);
    private SessionFactory factory;
    @Mock
    private Session session;
    @Mock
    private Transaction transaction;

    @InjectMocks // new HibernateChannelDao(factory);
    private HibernateChannelDao dao;

    @BeforeEach // запускается перед каждым методом @Test
    void setUp() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(factory.openSession()).thenReturn(session);
        Mockito.when(session.beginTransaction()).thenReturn(transaction);
    }

    @Test
    // @Test(expected = IllegalArgumentException.class)
    public void testCreateChannel_whenChannelWithNameExists_thenThrowException() {
        Query query = Mockito.mock(Query.class);
        // when(session.createQuery("", Channel.class)), то
        //  в методе findByName() при вызове метода session.createQuery() мы получим null
        Mockito.when(session.createQuery("from Channel where name = :channelName", Channel.class))
                .thenReturn(query);
        // ArgumentsMatchers
        Mockito.when(query.setParameter(eq("channelName"), any(String.class)))
                .thenReturn(query);

        List<Channel> channels = Collections.singletonList(new Channel());
        Mockito.when(query.getResultList()).thenReturn(channels);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> dao.createChannel("somename", "display name"));
        Mockito.verify(factory, Mockito.times(2)).openSession();
        Mockito.verify(transaction).rollback();
        Mockito.verify(session, Mockito.times(2)).close();
    }

    @Test
    public void testCreateChannel_whenChannelNameExistsAndUsingSpy_thenThrowException() {
        // spy objects - реальный объект (не mock), но у которого можно переопределить поведение некоторых методов
        HibernateChannelDao spyDao = Mockito.spy(dao);

        // Mockito.when(spyDao.findByName("somename")).thenReturn(new Channel());
        Channel channel = new Channel();
        channel.setName("somename");
        Mockito.doReturn(channel).when(spyDao).findByName("somename");

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> spyDao.createChannel("somename", "display name"));

        Mockito.verify(spyDao).findByName("somename");
        Mockito.verify(factory).openSession();
        Mockito.verify(transaction).rollback();
        Mockito.verify(session).close();
    }

}