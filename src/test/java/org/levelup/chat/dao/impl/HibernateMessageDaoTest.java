package org.levelup.chat.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.levelup.chat.domain.Channel;
import org.levelup.chat.domain.User;
import org.mockito.Mockito;

public class HibernateMessageDaoTest {

    private SessionFactory factory;

    private HibernateMessageDao messageDao;

//    @Test
//    public void test() {
//        messageDao = new HibernateMessageDao(new StubSessionFactory());
//
//        messageDao.sendMessage(new User(), new Channel(), "text");
//    }

    @BeforeEach // - этот метод (помеченный аннотацией BeforeEach будет вызываться перед каждым тестом (тестовым методом)
    public void setupMocks() {
        // mock object
        //  - все методы, которые имеют тип возращаемого значения (т.е. не null) -
        // то они возвращают значения по умолчанию (0, false or null)
        //  - мы можем переопределить поведение mock object для конкретного метода (конкретной ситуации)
        factory = Mockito.mock(SessionFactory.class);

        messageDao = new HibernateMessageDao(factory);
    }

    @Test
    public void testSendMessage() {
        Session session = Mockito.mock(Session.class);
        Transaction transaction = Mockito.mock(Transaction.class);
        // переопределяем поведение метода openSession у класса SessionFactory
        Mockito.when(factory.openSession()).thenReturn(session);

        // @Override public Session openSession() {
        //      return session;
        // }
        Mockito.when(session.beginTransaction()).thenReturn(transaction);

        messageDao.sendMessage(new User(), new Channel(), "some text");
        // Mockito.when(factory.openSession()).thenReturn(null);
        // messageDao.sendMessage(null, null, null);

        Mockito.verify(transaction).commit();
        Mockito.verify(session).close();
    }

}