package org.levelup.chat.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.levelup.chat.HibernateUtilsTest;
import org.levelup.chat.domain.Channel;
import org.levelup.chat.domain.Message;
import org.levelup.chat.domain.User;
import org.opentest4j.AssertionFailedError;

import java.util.List;

public class HibernateMessageDaoIT {

    private static SessionFactory factory;
    private static HibernateMessageDao dao;

    @BeforeAll // этот метод будет вызван только один раз в самом начале теста
    public static void setupObjects() {
        factory = HibernateUtilsTest.getSessionFactory();
        dao = new HibernateMessageDao(factory);
    }

    @Test
    public void testSendMessage_whenDataIsValid_thenCreateMessageInDB() {
        User user = createUser("login1", "firstName1", "lastName1");
        Channel channel = createChannel("channel1");

        dao.sendMessage(user, channel, "some message");

        try (Session session = factory.openSession()) {
            List<Message> messages = session.createQuery("from Message", Message.class)
                    .getResultList();
            Message result = messages.stream()
                    .filter(message ->
                            message.getUser().getLogin().equals(user.getLogin()) && message.getChannel().getName().equals(channel.getName())
                    )
                    .findFirst()
                    .orElseThrow(() -> new AssertionFailedError("Message wasn't created."));
            Assertions.assertEquals("some message", result.getText());
        }
    }

    private User createUser(String login, String firstName, String lastName) {
        try (Session session = factory.openSession()) {
            Transaction tx = session.beginTransaction();
            User user = new User(login, firstName, lastName);
            session.persist(user);
            tx.commit();
            return user;
        }
    }

    private Channel createChannel(String name) {
        try (Session session = factory.openSession()) {
            Transaction tx = session.beginTransaction();

            Channel channel = new Channel();
            channel.setName(name);
            channel.setDisplayName("channelDisplayName1");

            session.persist(channel);

            tx.commit();
            return channel;
        }
    }

}
